package com.psr.service.dashboard;

import com.psr.dto.DashboardDto;

public interface DashBoardService {
	
	/**
	 * get user id.
	 * 
	 * @param userId userid
	 * @return dashboard data
	 */
	public DashboardDto getDashboardData(Long userId);
}
