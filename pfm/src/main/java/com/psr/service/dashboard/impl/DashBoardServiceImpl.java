package com.psr.service.dashboard.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psr.dto.DashboardDto;
import com.psr.dto.IncomeDto;
import com.psr.dto.LiabilityDto;
import com.psr.dto.AssertDto;
import com.psr.entity.IncomeHistoryEntity;
import com.psr.entity.LiabilitiesEntity;
import com.psr.entity.assetsEntity;
import com.psr.repository.AssetsRepository;
import com.psr.repository.IncomeHistoryRepository;
import com.psr.service.dashboard.DashBoardService;
import com.psr.repository.LiabilitiesRepository;

@Service
public class DashBoardServiceImpl implements DashBoardService {
	
	@Autowired
	private AssetsRepository assetsRepository;
	
	@Autowired
	private LiabilitiesRepository liabilitiesRepository;
	
	@Autowired
	private IncomeHistoryRepository incomeHistroyRepository;

	@Override
	public DashboardDto getDashboardData(Long userId) {
		
		Double assertTotal[] = { 0.0 };
		Double liabTotal[] = { 0.0 };
		
		List<assetsEntity> assertEntityList = assetsRepository.findAssertUserId(userId);
		List<LiabilitiesEntity> libsList = liabilitiesRepository.findLibByUserId(userId);
		List<IncomeHistoryEntity> incomesList = incomeHistroyRepository.findIncomesByUserId(userId);
		
		List<AssertDto> assertList = new ArrayList<>();
		List<LiabilityDto> LiabilityList = new ArrayList<>();
		List<IncomeDto> incomeDto = new ArrayList<>();
		
		incomesList.stream().forEach(i -> {
			assertTotal[0] += Optional.ofNullable(i.getPrice()).orElse(0.0);
			incomeDto.add(IncomeDto.builder().item(i.getItem()).price(i.getPrice()).registDate(i.getRegistDate()).build());
		});
		
		assertEntityList.stream().forEach(i -> {
			assertTotal[0] += Optional.ofNullable(i.getPrice()).orElse(0.0);
			assertList.add(AssertDto.builder()
					.item(i.getItem())
					.price(i.getPrice())
					.registDate(i.getRegistDate())
					.build());
		});
		
		libsList.stream().forEach(b -> {
			liabTotal[0] += Optional.ofNullable(b.getPrice()).orElse(0.0);
			LiabilityList.add(LiabilityDto.builder()
					.item(b.getItem())
					.price(b.getPrice())
					.regist_date(b.getRegistTime())
					.build());
		});
		
		return DashboardDto.builder()
				.assertTotal(assertTotal[0])
				.liabTotal(liabTotal[0])
				.assets(assertList)
				.liabilities(LiabilityList)
				.income(incomeDto)
				.build();
	}

}
