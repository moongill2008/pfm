package com.psr.service.income.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psr.dto.IncomeDto;
import com.psr.entity.IncomeHistoryEntity;
import com.psr.repository.IncomeHistoryRepository;
import com.psr.web.Interface.input.IncomeInterface;

@Service(value = "incomeService")
public class incomeImpl implements IncomeInterface {
	
	@Autowired
	private IncomeHistoryRepository incomeHistoryRepository;
	
	@Override
	public List<IncomeDto> list(Long userId) {
		List<IncomeHistoryEntity> incomeHistoryEntityList = incomeHistoryRepository.findIncomesByUserId(userId);
		List<IncomeDto> incomeDtoList = new ArrayList<IncomeDto>();
		incomeHistoryEntityList.stream().forEach(dto -> {
			IncomeDto incomeDto = IncomeDto.builder()
					.id(dto.getId())
					.item(dto.getItem())
					.price(dto.getPrice())
					.registDate(dto.getRegistDate())
					.build();
			incomeDtoList.add(incomeDto);
		});
		return incomeDtoList;
	}

	@Override
	public void insert(IncomeDto income, Long userId) throws ParseException {
		IncomeHistoryEntity incomeHistoryEntity = IncomeHistoryEntity.builder()
		.item(income.getItem())
		.price(income.getPrice())
		.registDate(income.getRegistDate())
		.userId(userId)
		.build();

		incomeHistoryRepository.save(incomeHistoryEntity);
		
	}

	@Override
	public void remove(Long id) {
		incomeHistoryRepository.deleteById(id);
	}

	@Override
	public void update(IncomeHistoryEntity income) {
		IncomeHistoryEntity entity = incomeHistoryRepository.getOne(income.getId());
		entity.setItem(income.getItem());
		entity.setPrice(income.getPrice());
		incomeHistoryRepository.save(entity);
	}
}
