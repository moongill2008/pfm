package com.psr.service.expends.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psr.entity.ExpendsHistoryEntity;
import com.psr.repository.ExpendsHistoryRepository;
import com.psr.web.Interface.expends.expendsServiceInterface;

@Service
public class expendImpl implements expendsServiceInterface {
	
	@Autowired
	private ExpendsHistoryRepository expendsHistoryRepository;

	@Override
	public List<ExpendsHistoryEntity> list(Long userid) {
		return expendsHistoryRepository.findExpendsByUserId(userid);	
	}

	@Override
	public void insert(ExpendsHistoryEntity expend, Long userId) {
		expend.setUserId(userId);
		expendsHistoryRepository.save(expend);
	}

	@Override
	public void update(ExpendsHistoryEntity expend) {
		ExpendsHistoryEntity entity = expendsHistoryRepository.getOne(expend.getId());
		entity.setItem(expend.getItem());
		entity.setPrice(expend.getPrice());
		expendsHistoryRepository.save(entity);
	}

	@Override
	public void remove(Long id) {
		expendsHistoryRepository.deleteById(id);
	}

}
