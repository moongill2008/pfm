package com.psr.service.invest.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psr.dto.InvestDto;
import com.psr.entity.InvestEntity;
import com.psr.repository.InvestRepository;
import com.psr.web.Interface.invest.InvestServiceInterface;

@Service(value = "investService")
public class InvestServiceInterfaceImpl implements InvestServiceInterface {

	@Autowired
	private InvestRepository investRepository;

	@Override
	public List<InvestDto> list(Long userid) {
		List<InvestEntity> investList = investRepository.findByUserId(userid);
		
		List<InvestDto> investDtoList = new ArrayList<InvestDto>();
		if (!investList.isEmpty()) {
			investList.stream().forEach(f -> {
				InvestDto investDto = InvestDto.builder()
						.dateTime(f.getInsert_date())
						.input(f.getInput())
						.output(f.getOutput())
						.earn(f.getEarn())
						.lost(f.getLost())
						.build();
				
				investDtoList.add(investDto);
			});
		}
		return investDtoList;
	}

	@Override
	public void insert(InvestDto investDto, Long userId) {
		// dto to entity
		InvestEntity investEntity = InvestEntity.builder()
				.input(investDto.getInput())
				.output(investDto.getOutput())
				.earn(investDto.getEarn())
				.lost(investDto.getLost())
				.is_active(true)
				.insert_date(investDto.getDateTime())
				.userid(userId)
				.build();
		investRepository.save(investEntity);
	}

	@Override
	public void update(InvestDto investDto) {

	}

	@Override
	public void remove(InvestDto investDto) {
		// TODO Auto-generated method stub

	}

}
