package com.psr.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="invest")
public class InvestEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@NotNull
	@Column(name="input", nullable=false)
	private String input;
	
	@NotNull
	@Column(name="output", nullable=false)
	private String output;
	
	@Column(name="earn", nullable=false)
	private String earn;
	
	@Column(name="lost", nullable=false)
	private String lost;
	
	@Column(name="insert_date", nullable=false)
	private LocalDateTime insert_date;
	
	@Column(name="update_date", nullable=false)
	private LocalDateTime update_date;
	
	@Column(name="is_active", nullable=false)
	private boolean is_active;
	
	@Column(name="userid", nullable=false)
	private Long userid;
	
}
