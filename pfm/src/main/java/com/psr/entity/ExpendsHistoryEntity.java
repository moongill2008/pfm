package com.psr.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "expend_history")
public class ExpendsHistoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@NotNull
	@Column(name = "price")
	private Double price;
	
	@NotNull
	@Column(name = "item")
	private String item;

	@Column(name = "user")
	private Long userId;

	@Column(name = "regist_time")
	private LocalDateTime registDate;

	@Column(name = "update_time")
	private LocalDateTime updateDate;

	@Column(name = "delete_time")
	private LocalDateTime deleteDate;

}
