package com.psr.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "assets")
public class assetsEntity {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "userid", nullable = false)
    private Long userid;
    
    @Column(name = "item", nullable = false)
    private String item;
    
    @Column(name = "price", nullable = false)
    private Double price;
        
    @Column(name="regist_time")
    private LocalDateTime registDate;
    
    @Column(name="update_time")
    private LocalDateTime updateDate;
    
    @Column(name="delete_time")
    private LocalDateTime deleteDate;

}
