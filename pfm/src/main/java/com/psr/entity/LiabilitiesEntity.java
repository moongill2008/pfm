package com.psr.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Liability")
public class LiabilitiesEntity {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "item")
    private String item;
    
    @Column(name = "price")
    private Double price;
    
    @Column(name = "user")
    private Long userId;
    
    @Column(name="regist_time")
    private LocalDateTime registTime;
    
    @Column(name="update_time")
    private LocalDateTime updateTime;
    
    @Column(name="delete_time")
    private LocalDateTime deleteTime;

}
