package com.psr;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackageClasses = {
		PsrServerApplication.class,
//		Jsr310JpaConverters.class
})
public class PsrServerApplication {
	
	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

    public static void main(String[] args) {
        SpringApplication.run(PsrServerApplication.class, args);
    }
}
