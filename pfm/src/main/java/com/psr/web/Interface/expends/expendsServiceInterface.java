package com.psr.web.Interface.expends;

import java.util.List;

import com.psr.entity.ExpendsHistoryEntity;

public interface expendsServiceInterface {
	public List<ExpendsHistoryEntity> list(Long userid);
	public void insert(ExpendsHistoryEntity expend, Long userId);
	public void update(ExpendsHistoryEntity expend);
	public void remove(Long expend);

}
