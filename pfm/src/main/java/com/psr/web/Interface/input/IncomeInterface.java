package com.psr.web.Interface.input;

import java.text.ParseException;
import java.util.List;

import com.psr.dto.IncomeDto;
import com.psr.entity.IncomeHistoryEntity;

public interface IncomeInterface {
	public List<IncomeDto> list(Long id);
	public void insert(IncomeDto income, Long userId) throws ParseException;
	public void update(IncomeHistoryEntity income);
	public void remove(Long id);
}
