package com.psr.web.Interface.invest;

import java.util.List;

import com.psr.dto.InvestDto;

public interface InvestServiceInterface {
	public List<InvestDto> list(Long userid);
	public void insert(InvestDto investDto, Long userId);
	public void update(InvestDto investDto);
	public void remove(InvestDto investDto);
}
