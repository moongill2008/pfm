package com.psr.web.controller.expends;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psr.entity.ExpendsHistoryEntity;
import com.psr.repository.ExpendsHistoryRepository;
import com.psr.web.Interface.expends.expendsServiceInterface;
import com.psr.web.controller.base.baseController;

@RestController
@RequestMapping(value = "/expends")
public class expendsController extends baseController {
	
	@Autowired
	private expendsServiceInterface expendsService;
	
	@Autowired
	private ExpendsHistoryRepository expendsHistoryRepository;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<ExpendsHistoryEntity> getExpendsList() {
		return expendsService.list(getLoggedUser().getId());
	}
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public void insert(@Validated @RequestBody ExpendsHistoryEntity entity) {
		expendsService.insert(entity, getLoggedUser().getId());
	}
	
	@RequestMapping(value = "/detail/{id}", method=RequestMethod.GET)
	public ExpendsHistoryEntity getExpendsById(@PathVariable("id") long id) {
		return expendsHistoryRepository.findById(id).get();
	}
	
	@RequestMapping(value="/update", method=RequestMethod.PUT)
	public void updateById(@Validated @RequestBody ExpendsHistoryEntity entity) {
		expendsService.update(entity);
	}
	
	@RequestMapping(value="/remove/{id}", method=RequestMethod.DELETE)
	public void removeById(@PathVariable("id") long id) {
		expendsHistoryRepository.deleteById(id);
	}

}
