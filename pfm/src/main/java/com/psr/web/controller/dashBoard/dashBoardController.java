package com.psr.web.controller.dashBoard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psr.dto.DashboardDto;
import com.psr.service.dashboard.DashBoardService;
import com.psr.web.controller.base.baseController;

@RestController
@RequestMapping(value = "/dashboard")
public class dashBoardController extends baseController {

	@Autowired
	private DashBoardService dashBoardService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public DashboardDto getDashBoardData() {
		return dashBoardService.getDashboardData(getLoggedUser().getId());
	}

}
