package com.psr.web.controller.income;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psr.dto.IncomeDto;
import com.psr.entity.IncomeHistoryEntity;
import com.psr.repository.IncomeHistoryRepository;
import com.psr.web.Interface.input.IncomeInterface;
import com.psr.web.controller.base.baseController;

@CrossOrigin
@RestController
@RequestMapping(value="/income")
public class incomeController extends baseController {
	
	@Autowired
	private IncomeInterface incomeService;
	
	@Autowired
	private IncomeHistoryRepository incomeRepository;
	
	@RequestMapping(value="/list", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> list() {
		final List<IncomeDto> emptyList = incomeService.list(getLoggedUser().getId());
		return new ResponseEntity<Object>(emptyList, HttpStatus.OK);
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public ResponseEntity<Object> insert(@RequestBody IncomeDto inputDto) throws ParseException{
		incomeService.insert(inputDto, getLoggedUser().getId());
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/detail/{id}", method=RequestMethod.GET)
	public IncomeHistoryEntity getById(@PathVariable("id") long id) {
		return incomeRepository.findById(id).get();
	}
	
	@RequestMapping(value="/update", method=RequestMethod.PUT)
	public void updateById(@Validated @RequestBody IncomeHistoryEntity entity) {
		incomeService.update(entity);
	}
	
	@RequestMapping(value="/remove/{id}", method=RequestMethod.DELETE)
	public void removeById(@PathVariable("id") long id) {
		incomeRepository.deleteById(id);
	}

}
