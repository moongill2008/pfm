package com.psr.web.controller.invest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psr.dto.InvestDto;
import com.psr.web.Interface.invest.InvestServiceInterface;
import com.psr.web.controller.base.baseController;

@CrossOrigin
@RestController
@RequestMapping(value="/invest")
public class investController extends baseController {
	
	@Autowired
	private InvestServiceInterface investServiceInterface;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public List<InvestDto> list() {
		return investServiceInterface.list(getLoggedUser().getId());
	}

	@RequestMapping(value="/add", method=RequestMethod.POST)
	public void insert(@RequestBody InvestDto investDto) {
		investServiceInterface.insert(investDto, getLoggedUser().getId());
	}

	@RequestMapping(value="/update", method=RequestMethod.PUT)
	public void update() {
		
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.DELETE)
	public void delete() {

	}
}
