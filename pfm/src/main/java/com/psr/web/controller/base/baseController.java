package com.psr.web.controller.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.psr.security.UserPrincipal;

@Controller
public abstract class baseController {
	
	private Logger log = LoggerFactory.getLogger(baseController.class);
	
	/**
	 * get user info and return it
	 * 
	 * @return userprincipal
	 */
    public UserPrincipal getLoggedUser() {
		UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		log.info("logind user is:" + userPrincipal.getUsername());
		return userPrincipal;
    }
    
}
