package com.psr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.psr.entity.assetsEntity;

@Repository
public interface AssetsRepository extends JpaRepository<assetsEntity, Long> {
	
	@Query("SELECT r FROM assetsEntity r WHERE r.userid = :userId")
	List<assetsEntity> findAssertUserId(@Param("userId") Long userId);

}
