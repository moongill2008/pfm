package com.psr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psr.entity.expendEntity;

@Repository
public interface ExpendsRepository extends JpaRepository<expendEntity, Long> {

}
