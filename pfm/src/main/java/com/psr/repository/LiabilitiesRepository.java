package com.psr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.psr.entity.LiabilitiesEntity;

@Repository
public interface LiabilitiesRepository extends JpaRepository<LiabilitiesEntity, Long> {
	
	/** get lib by user */
	List<LiabilitiesEntity> findLibByUserId(@Param("userId") Long userId);

}
