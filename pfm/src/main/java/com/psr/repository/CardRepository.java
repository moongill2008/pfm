package com.psr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psr.entity.cardsEntity;

@Repository
public interface CardRepository extends JpaRepository<cardsEntity, Long> {

}
