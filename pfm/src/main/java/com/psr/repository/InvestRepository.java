package com.psr.repository;

import org.springframework.stereotype.Repository;

import com.psr.entity.InvestEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface InvestRepository extends JpaRepository<InvestEntity, Long>{
	
	@Query("SELECT i FROM InvestEntity i WHERE i.userid = :userId")
	List<InvestEntity> findByUserId(@Param("userId") Long userId);

}
