package com.psr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.psr.entity.ExpendsHistoryEntity;

@Repository
public interface ExpendsHistoryRepository extends JpaRepository<ExpendsHistoryEntity, Long> {
	
	@Query("SELECT r FROM ExpendsHistoryEntity r WHERE r.userId = :userId")
	List<ExpendsHistoryEntity> findExpendsByUserId(@Param("userId") Long userId);

}
