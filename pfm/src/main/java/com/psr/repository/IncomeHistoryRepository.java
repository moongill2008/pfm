package com.psr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.psr.entity.IncomeHistoryEntity;

@Repository
public interface IncomeHistoryRepository extends JpaRepository<IncomeHistoryEntity, Long> {

	@Query("SELECT r FROM IncomeHistoryEntity r WHERE r.userId = :userId")
	List<IncomeHistoryEntity> findIncomesByUserId(@Param("userId") Long userId);
}
