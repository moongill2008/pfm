package com.psr.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IncomeDto {
	private Long id;
	
	@NotNull
	private String item;
	
	@NotNull
	private Double price;
	
	@NotNull
	private LocalDateTime registDate;
}
