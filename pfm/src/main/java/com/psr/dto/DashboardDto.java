package com.psr.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DashboardDto {

	private Double assertTotal;

	private Double liabTotal;

	private List<AssertDto> assets;

	private List<LiabilityDto> liabilities;
	
	private List<IncomeDto> income;

}
