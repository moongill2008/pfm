package com.psr.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvestDto {
	@NotNull
	private LocalDateTime dateTime;
	
	@NotNull
	private String input;
	
	@NotNull
	private String output;
	
	@NotNull
	private String earn;
	
	@NotNull
	private String lost;
}
